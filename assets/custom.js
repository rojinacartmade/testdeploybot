$(document).ready(function(){
  	//Description to FAQ
  	//add arrows inform of questions
    function initialheadingstate(){
      $('.faq-listing h4').each(function(){
          $(this).html('<i class="fa fa-plus"></i> '+$(this).text());
      });
    }
  	initialheadingstate();
	$('.faq-listing h4').click(function(){
      if(!$(this).hasClass('active')){
        $('.faq-listing p').hide(); //hide all the answers if any is open
		$('.faq-listing h4').removeClass('active');
        initialheadingstate();
        $(this).addClass('active').next('p').slideToggle();
      }else{
		$('.faq-listing p').hide();
		$('.faq-listing h4').removeClass('active');
      }
      //maintain plus minus state
      $(this).find('.fa').toggleClass('fa-plus');
      $(this).find('.fa').toggleClass('fa-minus');
    });
});
/* Add to cart Ajax */
function updateminicart(){
  jQuery.getJSON('/cart.js', function(cart){
    console.log(cart.item_count);
      $('#CartCount').removeClass('hide');
      $('.site-header__cart-count').find('span').text(cart.item_count);
  });
}

function addtocartajax(ele,qty,variantid){
  var item = {'quantity':qty,'id':variantid};
  jQuery.ajax({
    url: '/cart/add.js',
    dataType: 'json',
    type: 'post',
    data: item,
    beforeSend: function(){
      jQuery(ele).text('adding...');
    },success: function(cart){
      //update the header cart count
      updateminicart();
    },
    complete: function(){
      jQuery(ele).html('&#10004; added!');
    },
    error :function( jqXhr ) {
      //outof stock or higher stock added in cart handle
      if( jqXhr.status === 422 ) {
        var response = jqXhr.responseText;
        response = jQuery.parseJSON(response);
        var message = response.message;
        var description = response.description;
        alert(message+' : '+description);
      }
    }//error: Shopify.addItemstoTheCart
  });

  setTimeout(function(){
    jQuery(ele).text('THANK YOU!');
  }, 2000);
  
  setTimeout(function(){
    jQuery(ele).text('ADD TO CART');
  }, 5000);

  return false;
}